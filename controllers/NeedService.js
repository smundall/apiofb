'use strict';

exports.changeNeed = function(args, res, next) {
  /**
   * parameters expected in the args:
  * needId (Integer)
  * status (Integer)
  * name (String)
  * leader (Integer)
  * permision (Integer)
  * completed (Integer)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

exports.createNeed = function(args, res, next) {
  /**
   * parameters expected in the args:
  * body (Need)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

exports.getNeedHelpers = function(args, res, next) {
  /**
   * parameters expected in the args:
  * needId (Integer)
  **/
  
  
  var examples = {};
  examples['application/json'] = [ {
  "need_id" : "aeiou",
  "user_id" : "aeiou",
  "user_name" : "aeiou",
  "id" : 123
} ];
  
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
  
}

exports.getNeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * query (String)
  * need (Integer)
  **/
  
  
  var examples = {};
  examples['application/json'] = [ {
  "permision_received" : true,
  "summary" : "aeiou",
  "address" : "aeiou",
  "description" : "aeiou",
  "last_name" : "aeiou",
  "leader_id" : 123,
  "creation_date" : 123,
  "photos" : "aeiou",
  "phone" : "aeiou",
  "creator_id" : 123,
  "completion_date" : 123,
  "id" : 123,
  "postal_code" : "aeiou",
  "first_name" : "aeiou",
  "status" : 1.3579000000000001069366817318950779736042022705078125
} ];
  
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
  
}

exports.updateNeed = function(args, res, next) {
  /**
   * parameters expected in the args:
  * body (Need)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

exports.uploadPhoto = function(args, res, next) {
  /**
   * parameters expected in the args:
  * needId (Integer)
  * file (file)
  * additionalMetadata (String)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

