'use strict';

var url = require('url');


var Church = require('./ChurchService');


module.exports.changeChurch = function changeChurch (req, res, next) {
  Church.changeChurch(req.swagger.params, res, next);
};

module.exports.createChurch = function createChurch (req, res, next) {
  Church.createChurch(req.swagger.params, res, next);
};

module.exports.getChurches = function getChurches (req, res, next) {
  Church.getChurches(req.swagger.params, res, next);
};

module.exports.updateChurch = function updateChurch (req, res, next) {
  Church.updateChurch(req.swagger.params, res, next);
};
