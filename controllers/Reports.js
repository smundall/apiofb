'use strict';

var url = require('url');


var Reports = require('./ReportsService');


module.exports.getReports = function getReports (req, res, next) {
  Reports.getReports(req.swagger.params, res, next);
};
