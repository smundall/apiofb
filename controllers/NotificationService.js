'use strict';

exports.createNotification = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
  // no response value expected for this operation
  
  
  res.end();
}

exports.deleteNotification = function(args, res, next) {
  /**
   * parameters expected in the args:
  * notificationId (Integer)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

exports.getNotifications = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
  
  
  var examples = {};
  examples['application/json'] = [ {
  "payload" : "aeiou",
  "user_id" : "aeiou",
  "action" : "aeiou",
  "creation_date" : "aeiou",
  "id" : 123,
  "type" : 123,
  "read_date" : 123
} ];
  
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
  
}

exports.updateNotification = function(args, res, next) {
  /**
   * parameters expected in the args:
  * body (Notification)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

