'use strict';

var url = require('url');


var Need = require('./NeedService');


module.exports.changeNeed = function changeNeed (req, res, next) {
  Need.changeNeed(req.swagger.params, res, next);
};

module.exports.createNeed = function createNeed (req, res, next) {
  Need.createNeed(req.swagger.params, res, next);
};

module.exports.getNeedHelpers = function getNeedHelpers (req, res, next) {
  Need.getNeedHelpers(req.swagger.params, res, next);
};

module.exports.getNeeds = function getNeeds (req, res, next) {
  Need.getNeeds(req.swagger.params, res, next);
};

module.exports.updateNeed = function updateNeed (req, res, next) {
  Need.updateNeed(req.swagger.params, res, next);
};

module.exports.uploadPhoto = function uploadPhoto (req, res, next) {
  Need.uploadPhoto(req.swagger.params, res, next);
};
