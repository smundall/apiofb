'use strict';

var url = require('url');


var Purchase = require('./PurchaseService');


module.exports.getPurchases = function getPurchases (req, res, next) {
  Purchase.getPurchases(req.swagger.params, res, next);
};

module.exports.purchaseAndroid = function purchaseAndroid (req, res, next) {
  Purchase.purchaseAndroid(req.swagger.params, res, next);
};

module.exports.purchaseApple = function purchaseApple (req, res, next) {
  Purchase.purchaseApple(req.swagger.params, res, next);
};
