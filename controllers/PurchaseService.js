'use strict';

exports.getPurchases = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
  
  
  var examples = {};
  examples['application/json'] = [ {
  "payment_type" : "aeiou",
  "receipt_confirmation" : "aeiou",
  "currency_type" : "aeiou",
  "user_id" : 123,
  "payment_amount" : 123,
  "creation_date" : "aeiou",
  "id" : 123,
  "payment_method" : "aeiou"
} ];
  
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
  
}

exports.purchaseAndroid = function(args, res, next) {
  /**
   * parameters expected in the args:
  * body (Android)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

exports.purchaseApple = function(args, res, next) {
  /**
   * parameters expected in the args:
  * body (Apple)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

