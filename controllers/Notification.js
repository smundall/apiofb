'use strict';

var url = require('url');


var Notification = require('./NotificationService');


module.exports.createNotification = function createNotification (req, res, next) {
  Notification.createNotification(req.swagger.params, res, next);
};

module.exports.deleteNotification = function deleteNotification (req, res, next) {
  Notification.deleteNotification(req.swagger.params, res, next);
};

module.exports.getNotifications = function getNotifications (req, res, next) {
  Notification.getNotifications(req.swagger.params, res, next);
};

module.exports.updateNotification = function updateNotification (req, res, next) {
  Notification.updateNotification(req.swagger.params, res, next);
};
