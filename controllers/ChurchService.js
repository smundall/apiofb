'use strict';

exports.changeChurch = function(args, res, next) {
  /**
   * parameters expected in the args:
  * churchId (Integer)
  * status (Integer)
  * name (String)
  * phone (String)
  * admin (Integer)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

exports.createChurch = function(args, res, next) {
  /**
   * parameters expected in the args:
  * body (Church)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

exports.getChurches = function(args, res, next) {
  /**
   * parameters expected in the args:
  * query (String)
  * church (Integer)
  **/
  
  console.log("Getting churches!!1");
  var examples = {};
  examples['application/json'] = [ {
  "address" : "aeiou",
  "phone" : "aeiou",
  "admin_id" : 123,
  "description" : "aeiou",
  "id" : 123,
  "postal_code" : "aeiou",
  "status" : 1.3579000000000001069366817318950779736042022705078125
} ];
  
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));

  
}

exports.updateChurch = function(args, res, next) {
  /**
   * parameters expected in the args:
  * body (Church)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

