'use strict';

exports.changeUser = function(args, res, next) {
  /**
   * parameters expected in the args:
  * userId (Integer)
  * status (String)
  * church (Integer)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

exports.createUser = function(args, res, next) {
  /**
   * parameters expected in the args:
  * body (User)
  **/
  
  
  var examples = {};
  examples['application/json'] = "aeiou";
  
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
  
}

exports.getUser = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
  
  
  var examples = {};
  examples['application/json'] = {
  "address" : "aeiou",
  "church_id" : 123,
  "last_name" : "aeiou",
  "subscription_expiration" : 123,
  "creation_date" : 123,
  "subscription" : "aeiou",
  "password" : "aeiou",
  "phone" : "aeiou",
  "id" : 123,
  "postal_code" : "aeiou",
  "first_name" : "aeiou",
  "email" : "aeiou",
  "status" : 1.3579000000000001069366817318950779736042022705078125
};
  
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
  
}

exports.loginUser = function(args, res, next) {
  /**
   * parameters expected in the args:
  * body (Credentials)
  **/
  
  
  var examples = {};
  examples['application/json'] = {
  "message" : "aeiou",
  "status" : 123
};
  
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
  
}

exports.updateUser = function(args, res, next) {
  /**
   * parameters expected in the args:
  * body (User)
  **/
  // no response value expected for this operation
  
  
  res.end();
}

